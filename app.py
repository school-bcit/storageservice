import connexion
import os
from connexion import NoContent
import yaml
import time
import logging
from logging import config
from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker
from base import Base
from borrow_your_book import BorrowYourBook
from reserve_your_item import ReserveYourItem
import datetime
import json
from threading import Thread
from pykafka import KafkaClient
from pykafka.common import OffsetType

# Your functions here to handle your endpoints
if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yml"
    log_conf_file = "/config/log_conf.yml"
else:
    print("In Dev Environment")
    app_conf_file = "app_conf.yml"
    log_conf_file = "log_conf.yml"

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())

with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)

logger.info(f'Connecting to DB. Hostname: {app_config["datastore"]["hostname"]}, '
            f'Port: {app_config["datastore"]["port"]}')

DB_ENGINE = create_engine(f'mysql+pymysql://{app_config["datastore"]["user"]}:'
                          f'{app_config["datastore"]["password"]}@{app_config["datastore"]["hostname"]}:'
                          f'{app_config["datastore"]["port"]}/{app_config["datastore"]["db"]}')

Base.metadata.bind = DB_ENGINE
DB_SESSION = sessionmaker(bind=DB_ENGINE)


def process_messages():
    """ Process event messages """
    # hostname = "%s:%d" % (app_config["events"]["hostname"],
    #                       app_config["events"]["port"])
    hostname = f'{app_config["events"]["hostname"]}:{app_config["events"]["port"]}'

    max_connection_retry = app_config["events"]["max_retries"]
    current_retry_count = 0

    while current_retry_count < max_connection_retry:
        try:
            logger.info(f'[Storage][Retry #{current_retry_count}] Connecting to Kafka...')

            client = KafkaClient(hosts=hostname)
            topic = client.topics[str.encode(app_config["events"]["topic"])]
            break

        except:
            logger.error(f'[Storage] Connection to Kafka failed in retry #{current_retry_count}!')

            time.sleep(app_config["events"]["sleep"])
            current_retry_count += 1

    # client = KafkaClient(hosts=hostname)
    # topic = client.topics[str.encode(app_config["events"]["topic"])]

    # Create a consume on a consumer group, that only reads new messages
    # (uncommitted messages) when the service re-starts (i.e., it doesn't
    # read all the old messages from the history in the message queue).

    consumer = topic.get_simple_consumer(consumer_group=b'event_group',
                                         reset_offset_on_start=False,
                                         auto_offset_reset=OffsetType.LATEST)

    # This is blocking - it will wait for a new message
    for msg in consumer:
        print(msg)
        msg_str = msg.value.decode('utf-8')
        msg = json.loads(msg_str)
        logger.info("Message: %s" % msg)

        payload = msg["payload"]

        if msg["type"] == "bb":  # Change this to your event type
            # Store the event1 (i.e., the payload) to the DB
            # find_restaurant(payload)
            session = DB_SESSION()
            # find_restaurant(payload)

            bb = BorrowYourBook(payload['book_id'],
                                payload['book_name'],
                                payload['book_auther_name'],
                                payload['borrowed_date'],
                                payload['return_date'])

            session.add(bb)  # SQL insert statement
            session.commit()
            session.close()
            logger.debug(f'Stored event "Borrow Your Book" with a unique id of {payload["book_id"]}')

        elif msg["type"] == "ir":  # Change this to your event type
            # Store the event2 (i.e., the payload) to the DB
            # write_review(payload)
            session = DB_SESSION()

            ir = ReserveYourItem(payload['item_id'],
                                 payload['item_type'],
                                 payload['item_location'],
                                 payload['reserved_date'],
                                 payload['reservation_length'])

            session.add(ir)
            session.commit()
            session.close()
            logger.debug(f'Stored event "Reserve Your Item" with a unique id of {payload["item_id"]}')

        # Commit the new message as being read
        consumer.commit_offsets()


# def select_book_to_borrow(body):
#     """ Adds a book to the users profile to borrow """
#     # Implement Here
#     session = DB_SESSION()
#     bp = BorrowYourBook(body['book_id'],
#                         body['book_name'],
#                         body['book_auther_name'],
#                         body['borrowed_date'],
#                         body['return_date'])
#
#     session.add(bp)  # equivalent to SQL insert command or statement
#     session.commit()
#     session.close()
#     logger.debug("Stored event 'Borrow Your Book' request with a unique id of %s" % body["book_id"])
#
#     return NoContent, 201
#
#
# def reserve_item_to_use(body):
#     """ Adds a desk/computer to the users profile to reserve for future use """
#     # Implement Here
#     session = DB_SESSION()
#     bp = ReserveYourItem(body['item_id'],
#                          body['item_type'],
#                          body['item_location'],
#                          body['reserved_date'],
#                          body['reservation_length'])
#
#     session.add(bp)  # equivalent to SQL insert command or statement
#     session.commit()
#     session.close()
#     logger.debug("Stored event 'Reserve Your Item' request with a unique id of %s" % body["item_id"])
#
#     return NoContent, 201


def get_book_to_borrow(start_timestamp, end_timestamp):
    """ Gets new blood pressure readings after the timestamp """
    session = DB_SESSION()
    start_timestamp_datetime = datetime.datetime.strptime(start_timestamp, "%Y-%m-%dT%H:%M:%SZ")
    end_timestamp_datetime = datetime.datetime.strptime(end_timestamp, "%Y-%m-%dT%H:%M:%SZ")
    # timestamp_datetime = datetime.datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%SZ")
    book_list = session.query(BorrowYourBook).filter(and_(BorrowYourBook.date_created >= start_timestamp_datetime,
                                                          BorrowYourBook.date_created < end_timestamp_datetime))
    results_list = []

    for book in book_list:
        results_list.append(book.to_dict())
        session.close()
        logger.info("Query for Borrow Your Book list between %s and %s returns %d results" % (start_timestamp,

                  end_timestamp,

                  len(results_list)))
    return results_list, 200


def get_reserve_item_to_use(start_timestamp, end_timestamp):
    """ Gets new blood pressure readings after the timestamp """
    session = DB_SESSION()
    #start_timestamp = start_timestamp.strip()
    start_timestamp_datetime = datetime.datetime.strptime(start_timestamp.strip(), "%Y-%m-%dT%H:%M:%SZ")
    end_timestamp_datetime = datetime.datetime.strptime(end_timestamp.strip(), "%Y-%m-%dT%H:%M:%SZ")

    # timestamp_datetime = datetime.datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%SZ")
    reserve_list = session.query(ReserveYourItem).filter(and_(ReserveYourItem.date_created >= start_timestamp_datetime,
                                                              ReserveYourItem.date_created < end_timestamp_datetime))
    results_list = []

    for reserve in reserve_list:
        results_list.append(reserve.to_dict())
        session.close()
        logger.info("Query for Reserve Your Item list between %s and %s returns %d results" % (start_timestamp,

                   end_timestamp,

                   len(results_list)))
    return results_list, 200


app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi.yaml", strict_validation=True, validate_responses=True)

if __name__ == "__main__":
    t1 = Thread(target=process_messages)
    t1.setDaemon(True)
    t1.start()
    app.run(port=8090, host="0.0.0.0")
