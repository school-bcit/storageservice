import mysql.connector

db_conn = mysql.connector.connect(host="acit3855-setc-mahsa.eastus.cloudapp.azure.com", user="maryam", password="password", database="events")

db_cursor = db_conn.cursor()
db_cursor.execute('''
          CREATE TABLE borrow_your_book
          (id INT NOT NULL AUTO_INCREMENT, 
           book_id VARCHAR(250) NOT NULL,
           book_name VARCHAR(250) NOT NULL,
           book_auther_name VARCHAR(250) NOT NULL,
           borrowed_date VARCHAR(100) NOT NULL,
           return_date VARCHAR(100) NOT NULL,
           date_created VARCHAR(100) NOT NULL,
           CONSTRAINT borrow_your_book_pk PRIMARY KEY (id))
          ''')

db_cursor.execute('''
          CREATE TABLE reserve_your_item
          (id INT NOT NULL AUTO_INCREMENT, 
           item_id VARCHAR(250) NOT NULL,
           item_type VARCHAR(250) NOT NULL,
           item_location VARCHAR(250) NOT NULL,
           reserved_date VARCHAR(100) NOT NULL,
           reservation_length VARCHAR(100) NOT NULL,
           date_created VARCHAR(100) NOT NULL,
           CONSTRAINT heart_rate_pk PRIMARY KEY (id))
          ''')

db_conn.commit()
db_conn.close()
