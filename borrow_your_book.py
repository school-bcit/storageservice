import datetime
from sqlalchemy import Column, Integer, String, DateTime
from base import Base


class BorrowYourBook(Base):
    """ Books to Borrow For the Users """

    __tablename__ = "borrow_your_book"

    id = Column(Integer, primary_key=True)
    book_id = Column(String(250), nullable=False)
    book_name = Column(String(250), nullable=False)
    book_auther_name = Column(String(250), nullable=False)
    # timestamp = Column(String(100), nullable=False)
    date_created = Column(DateTime, nullable=False)
    borrowed_date = Column(String(100), nullable=False)
    return_date = Column(String(100), nullable=False)

    def __init__(self, book_id, book_name, book_auther_name, borrowed_date, return_date):
        """ Initializes a heart rate reading """
        self.book_id = book_id
        self.book_name = book_name
        self.book_auther_name = book_auther_name
        # self.timestamp = timestamp
        self.date_created = datetime.datetime.now()  # Sets the date/time record is created
        self.borrowed_date = borrowed_date
        self.return_date = return_date

    def to_dict(self):
        """ Dictionary Representation of a heart rate reading """
        dict = {}

        dict['id'] = self.id
        dict['book_id'] = self.book_id
        dict['book_name'] = self.book_name
        dict['book_auther_name'] = self.book_auther_name
        # dict['timestamp'] = self.timestamp
        dict['date_created'] = self.date_created
        dict['borrowed_date'] = self.borrowed_date
        dict['return_date'] = self.return_date

        return dict
