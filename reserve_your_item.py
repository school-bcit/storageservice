import datetime
from sqlalchemy import Column, Integer, String, DateTime
from base import Base


class ReserveYourItem(Base):
    """ Reserve a Desk/Computer Online to Study in a Library """

    __tablename__ = "reserve_your_item"

    id = Column(Integer, primary_key=True)
    item_id = Column(String(250), nullable=False)
    item_type = Column(String(250), nullable=False)
    # timestamp = Column(String(100), nullable=False)
    date_created = Column(DateTime, nullable=False)
    item_location = Column(String(250), nullable=False)
    reserved_date = Column(String(100), nullable=False)
    reservation_length = Column(String(100), nullable=False)

    def __init__(self, item_id, item_type, item_location, reserved_date, reservation_length):
        """ Initializes a blood pressure reading """
        self.item_id = item_id
        self.item_type = item_type
        # self.timestamp = timestamp
        self.date_created = datetime.datetime.now()  # Sets the date/time record is created
        self.item_location = item_location
        self.reserved_date = reserved_date
        self.reservation_length = reservation_length

    def to_dict(self):
        """ Dictionary Representation of a blood pressure reading """
        dict = {}
        dict['id'] = self.id
        dict['item_id'] = self.item_id
        dict['item_type'] = self.item_type
        # dict['blood_pressure'] = {}
        # dict['blood_pressure']['systolic'] = self.systolic
        # dict['blood_pressure']['diastolic'] = self.diastolic
        # dict['timestamp'] = self.timestamp
        dict['date_created'] = self.date_created
        dict['item_location'] = self.item_location
        dict['reserved_date'] = self.reserved_date
        dict['reservation_length'] = self.reservation_length

        return dict
